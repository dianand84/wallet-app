require 'rails_helper'

shared_examples_for "notifier" do
  let(:model) { described_class.new }

  describe '#notify_by_email' do
    it 'should notify via email' do
      mail_provider = double('an email provider')
      expect(mail_provider).to receive(:send_email).with('admin@mail.com', 'foo bar').and_return(true)
      model.notify_by_email(mail_provider, 'foo bar')
    end
  end

  describe '#notify_by_sms' do
    it 'should notify via sms on every transaction' do
      VCR.use_cassette('notify_by_sms') do
        response = model.notify_by_sms()
        expect(response['success']).to eq(true)
        expect(response['number']).to eq('99999999999')
        expect(response['message']).to eq('Message from application')
      end
    end
  end
end
