require 'rails_helper'

describe Wallet do
  it_behaves_like 'notifier'

  let(:wallet) { Wallet.create() }

  describe 'validations' do
    it { should validate_presence_of(:balance) }

    it "should throw up when balance is < 0" do
      wallet.balance = -1
      expect(wallet.save).to be_falsy
    end
  end

  describe '#deposit', :vcr do
    it 'should add the amount to the existing balance' do
      wallet.deposit(20)
      expect(wallet.balance).to eq(20)
    end

    context 'with a negative amount' do
      let(:wallet) { Wallet.create(balance: 500) }

      it 'should raise an error' do
        expect{
          wallet.deposit(-50)
        }.to raise_error("Invalid Amount")
        expect(wallet.balance).to eq(500)
      end
    end

    context 'with a string/char input' do
      let(:wallet) { Wallet.create(balance: 500) }

      it 'should raise an error' do
        expect{
          wallet.deposit('abc')
        }.to raise_error("Invalid Input")
        expect(wallet.balance).to eq(500)
      end
    end
  end

  describe '#withdraw', :vcr do
    let(:wallet) { Wallet.new(balance: 500) }
    it 'should deduct the amount from balance' do
      wallet.withdraw(50)
      expect(wallet.balance).to eq(450)
    end

    context 'with a negative amount' do
      it 'should raise an error' do
        expect{
          wallet.withdraw(-50)
        }.to raise_error('Invalid Amount')
        expect(wallet.balance).to eq(500)
      end
    end

    context 'with an amount greater than the balance' do
      it 'should raise an error' do
        expect{
          wallet.withdraw(600)
        }.to raise_error('Amount greater than balance')
        expect(wallet.balance).to eq(500)
      end
    end

    context 'on balance reaching zero' do
      it 'should notify' do
        expect(wallet).to receive(:notify_by_email).with(any_args)
        wallet.withdraw(500)
      end
    end

    context 'on withdraw' do
      it 'should notify via sms' do
        expect(wallet).to receive(:notify_by_sms).with(any_args)
        wallet.withdraw(500)
      end
    end

    context 'on deposit' do
      it 'should notify via sms' do
        expect(wallet).to receive(:notify_by_sms).with(any_args)
        wallet.deposit(500)
      end
    end
  end
end
