Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resource :wallet do
    post :deposit
    post :withdraw
  end
end
