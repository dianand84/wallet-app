Given(/^my balance is (\d+)$/) do |amount|
  wallet = Wallet.first
  wallet = Wallet.create unless wallet
  wallet.update_attributes(balance: amount)
end

When(/^I go to the wallet page$/) do
  visit wallet_path
end

When(/^I input (\d+) as the amount to be deposited$/) do |amount|
  within '.deposit' do
    fill_in 'amount', with: amount
  end
end

When(/^I input (\d+) as the amount to be withdrawn$/) do |amount|
  within '.withdraw' do
    fill_in 'amount', with: amount
  end
end

When(/^I click deposit$/) do
  within '.deposit' do
    click_button 'Deposit'
  end
end

When(/^I click Withdraw$/) do
  within '.withdraw' do
    click_button 'Withdraw'
  end
end

Then(/^I should see (\d+) as the balance$/) do |amount|
  expect(page).to have_content("Wallet Balance: #{amount}")
end

When(/^I input \-(\d+) as the amount to be deposited$/) do |amount|
  within '.deposit' do
    fill_in 'amount', with: "-#{amount}"
  end
end

When(/^I input \-(\d+) as the amount to be withdrawn$/) do |amount|
  within '.withdraw' do
    fill_in 'amount', with: "-#{amount}"
  end
end

Then(/^I should see an error message saying "([^"]*)"$/) do |message|
  within '.flash' do
    expect(page).to have_content(message)
  end
end
