Feature: Withdraw
  Scenario: Withdrawing amount
    Given my balance is 500
    When I go to the wallet page
    And I input 50 as the amount to be withdrawn
    And I click Withdraw
    Then I should see 450 as the balance
  Scenario: Withdrawing negative amount
    Given my balance is 500
    When I go to the wallet page
    And I input -50 as the amount to be withdrawn
    And I click Withdraw
    Then I should see 500 as the balance
    And I should see an error message saying "Invalid Amount"
  Scenario: Withdrawing amount greater than balance
    Given my balance is 500
    When I go to the wallet page
    And I input 600 as the amount to be withdrawn
    And I click Withdraw
    Then I should see 500 as the balance
    And I should see an error message saying "Amount greater than balance"
