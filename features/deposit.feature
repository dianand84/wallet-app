Feature: Deposit
  Scenario: Depositing Amount
    Given my balance is 0
      When I go to the wallet page
      And I input 42 as the amount to be deposited
      And I click deposit
      Then I should see 42 as the balance
  Scenario: Depositing negative amount
    Given my balance is 100
      When I go to the wallet page
      And I input -50 as the amount to be deposited
      And I click deposit
      Then I should see 100 as the balance
      And I should see an error message saying "Invalid Amount"
