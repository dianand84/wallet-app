class WalletsController < ApplicationController
  before_action :load_wallet

  def deposit
    amount = wallet_params[:amount]
    begin
      @wallet.deposit amount
    rescue RuntimeError => e
      flash[:error] = e.message
    end
    redirect_to wallet_path
  end

  def withdraw
    amount = wallet_params[:amount]
    begin
      @wallet.withdraw amount
    rescue RuntimeError => e
      flash[:error] = e.message
    end
    redirect_to wallet_path
  end

  private

  def load_wallet
    @wallet = Wallet.first
    @wallet = Wallet.create unless @wallet
  end

  def wallet_params
    params.permit(:amount)
  end

end
