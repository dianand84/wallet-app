class Wallet < ApplicationRecord
  include Notifier
  validates :balance, presence: true
  validate :positive_amount

  def deposit(amount)
    raise('Invalid Input') if (amount.to_f == 0 || amount.to_f == 0.0) && (amount != 0 || amount != 0.0)
    self.balance = 0 unless self.balance
    amount = amount.to_f
    raise('Invalid Amount') if amount < 0
    update_attributes(balance: (balance + amount))
    notify_by_sms()
  end

  def withdraw(amount)
    self.balance = 0 unless self.balance
    amount = amount.to_f
    raise('Invalid Amount') if amount < 0
    raise('Amount greater than balance') if amount > balance
    update_attributes(balance: (balance - amount))
    self.reload
    notify_by_email(nil, 'foo bar') if balance <= 0
    notify_by_sms()
  end

  private

  def positive_amount
    if self.balance && self.balance < 0
      errors.add(:balance, 'must be greater than or equal to 0')
    end
  end
end
