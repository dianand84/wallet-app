module Notifier
  extend ActiveSupport::Concern

  included do
    def notify_by_email(provider, message)
      provider.send_email('admin@mail.com', message) if provider
    end

    def notify_by_sms()
      response = RestClient.post 'https://private-7898f5-sendsms5.apiary-mock.com/sms', {number: '99999999', message: 'Message from application'}
      JSON.parse(response)
    end
  end
end
